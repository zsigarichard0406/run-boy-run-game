using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 5f; //Controls velocity multiplier
    public float forwardMove=5f;
    public float jumpForce=2f;
    Rigidbody rb; //Tells script there is a rigidbody, we can use variable rb to reference it in further script


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); //rb equals the rigidbody on the player
    }


    // Update is called once per frame
    void Update()
    {
        float sideMove = Input.GetAxis("Horizontal") * speed* Time.deltaTime; ; 

        rb.velocity = new Vector3(sideMove, 0, forwardMove) * speed; // Creates velocity in direction of value equal to keypress (WASD). rb.velocity.y deals with falling + jumping by setting velocity to y. 
        if(Input.GetKeyDown(KeyCode.Space))
        {
             rb.AddForce(Vector3.up*jumpForce);
             
        }
       
    

    }
}